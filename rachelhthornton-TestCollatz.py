#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(5,5)
        self.assertEqual(v, 6) 

    def test_eval_6(self):
        v = collatz_eval(9,10)
        self.assertEqual(v,20)

    def test_eval_7(self):
        v = collatz_eval(10,9)
        self.assertEqual(v,20) #I haven't written my program so that it can consider numbers in decending order, will fail

    #def test_eval_8(self):
        #v = collatz_eval(999999,1000000)
        #self.assertNotEqual(v,259) #259 is the actual highest value between these two, so if the assert pre conditions fails this will fail.
    # -----
    # print
    # -----

    def test_print_9(self):
        w = StringIO() #creates an empty string object we can write in!
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_10(self): #technically this function is defined for funtions outside of Collatz_eval(), so this should work
        w = StringIO() 
        collatz_print(w, 999999, 1000000, 259)
        self.assertEqual(w.getvalue(), "999999 1000000 259\n")

    def test_print_11(self):
        w = StringIO()
        collatz_print(w, 10, 9, 20)
        self.assertEqual(w.getvalue(), "10 9 20\n")

    # -----
    # solve
    # -----

    def test_solve_12(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_13(self):
        r = StringIO("9 10\n10 9\n5 5\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "9 10 20\n10 9 20\n5 5 6\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
