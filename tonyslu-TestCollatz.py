#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
        #
    def test_read2(self):
        s = "200 300\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 200)
        self.assertEqual(j, 300)
    def test_read3(self):
        s = "610 603\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 610)
        self.assertEqual(j, 603)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(13, 9876)
        self.assertEqual(v, 262)

    def test_eval_6(self):
        v = collatz_eval(349837, 349844)
        self.assertEqual(v, 154)

    def test_eval_7(self):
        v = collatz_eval(764839, 764900)
        self.assertEqual(v, 300)

    def test_eval_8(self):
        v = collatz_eval(500, 101)
        self.assertEqual(v, 144)

    def test_eval_9(self):
        v = collatz_eval(5000, 800)
        self.assertEqual(v, 238)

    def test_eval_10(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_11(self):
        v = collatz_eval(999998, 999997)
        self.assertEqual(v, 259)

    def test_eval_12(self):
        v = collatz_eval(43, 43)
        self.assertEqual(v, 30)

    def test_eval_13(self):
        v = collatz_eval(678, 715003)
        self.assertEqual(v, 509)
    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print2(self):
        w = StringIO()
        collatz_print(w, 10, 1, 20)
        self.assertEqual(w.getvalue(), "10 1 20\n")

    def test_print3(self):
        w = StringIO()
        collatz_print(w, 999999, 1, 1)
        self.assertEqual(w.getvalue(), "999999 1 1\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve2(self):
        r = StringIO("10 1\n200 100\n210 201\n1000 900\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "10 1 20\n200 100 125\n210 201 89\n1000 900 174\n")

    def test_solve3(self):
        r = StringIO("13 13\n5 5\n1 1\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "13 13 10\n5 5 6\n1 1 1\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
