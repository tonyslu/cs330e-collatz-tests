#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)
        z = collatz_eval(300, 401)
        self.assertEqual(z, 144)
        x = collatz_eval(100, 1)
        self.assertEqual(x, 119)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)
        z = collatz_eval(999999, 999998)
        self.assertEqual(z, 259)
        x = collatz_eval(1000, 900)
        self.assertEqual(x, 174)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)
        z = collatz_eval(999990, 999999)
        self.assertEqual(z, 259)
        x = collatz_eval(2, 1)
        self.assertEqual(x, 2)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)
        z = collatz_eval(1, 2)
        self.assertEqual(z, 2)
        x = collatz_eval(999999, 999842)
        self.assertEqual(x, 259)

        
    
    
    


    # -----
    # print
    # -----

    def test_print1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print2(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")
    def test_print3(self):
        w = StringIO()
        collatz_print(w, 201, 210, 89)
        self.assertEqual(w.getvalue(), "201 210 89\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n300 401\n100 1\n999999 999998\n1000 900\n999990 999999\n2 1\n1 2\n999999 999842\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n300 401 144\n100 1 119\n999999 999998 259\n1000 900 174\n999990 999999 259\n2 1 2\n1 2 2\n999999 999842 259\n")
    
    

# ----
# main
# ----

if __name__ == "__main__":
    main()
"""#pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
